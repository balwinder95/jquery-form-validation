$(document).ready(function(){
	var count,d,year,month,day;
	var now=new Date();

	var nameReg=/^[A-Z a-z \s \.]+$/;		//Regular expression for name
	var emailReg=/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;		//Regular expression for email
	var phoneReg=/^\([1-9][0-9]{2}\) [0-9]{3}-[0-9]{4}$/;			//Regular expression for phone no.
	$("#phone").mask("(999) 999-9999");
	//function executed when submit button is clicked
	$("#submit").click(function(){		
		d=$("#date").val();			//get value from datepicker
		year=d.substr(0,4);			//get year value from the selected date 
		month=d.substr(5,2);		//get month value from the selected date 
		day=d.substr(8,2);			//get day value from the selected date 
		count=0;					//reset count variable

		if($("#fname").val().length==0 || (!nameReg.test($("#fname").val()))){			//To check validity of first name
			$("#fname_error").show(500);	//if invalid display error message
		}
		else{
			$("#fname_error").hide(500);count++;    //if valid hide the error message and increment count
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#lname").val().length==0 || (!nameReg.test($("#lname").val()))){			//To check validity of last name
			$("#lname_error").show(500);
		}
		else{
			$("#lname_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if(d=="" || checkDate(year,month,day)==1){				//To check validity of date
			$("#date_error").show(500);
		}
		else{
			$("#date_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if ($("#male").is(":checked") || $("#female").is(":checked")){			//To check radio button is selected
			$("#gender_error").hide(500);count++;
		}
		else{
			$("#gender_error").show(500);
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#address").val().length==0){								//To check if address field is empty
			$("#address_error").show(500);
		}
		else{
			$("#address_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#phone").val().length==0 || (!phoneReg.test($("#phone").val()))){						//To check if phone no. field is valid
			$("#phone_error").show(500);
		}
		else{
			$("#phone_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#email").val().length==0 || (!emailReg.test($("#email").val()))){			//To check validity of email
			$("#email_error").show(500);
		}
		else{
			$("#email_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#password").val().length==0){						//To check password field is not empty
			$("#password_error").show(500);
		}
		else{
			$("#password_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if($("#confirm_password").val()!=$("#password").val()){			//To check confirm password is same as password
			$("#confirm_password_error").show(500);
		}
		else{
			$("#confirm_password_error").hide(500);count++;
		}
		//----------------------------------------------------------------------------------------------------------------
		if ($("#checkbox1").is(":checked") || $("#checkbox2").is(":checked") || $("#checkbox3").is(":checked") || $("#checkbox4").is(":checked")){ //To check atleast one checkbox is selected
			$("#hobbies_error").hide(500);count++;
		}
		else{
			$("#hobbies_error").show(500);
		}
		//----------------------------------------------------------------------------------------------------------------
		if(count==10){					//if all fields are validated navigate to other page
			window.location.href="submitted.html";
		}
	});
						
	//function to check selected date is not greater than the current date 
	//returns 1 if selected date is greater than the current date
	function checkDate(year,month,day){		
		if(year>now.getFullYear()){
			return 1;				
		}
		else if(year==now.getFullYear()){
			if(month>now.getMonth()+1){
				return 1;
			}
			else if(month==now.getMonth()+1){
				if(day>now.getDate()){
					return 1;
				}
			}
		}
	}
});